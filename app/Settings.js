import React from 'react';
import { StyleSheet, Text, View, Switch } from 'react-native';
import { MapView } from 'expo';

export default class Settings extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 15, fontWeight: 'bold', margin: 10}}>Settings</Text>
        
        <Text style={{fontSize: 50, fontWeight: 'bold'}}>AR</Text>
        
        <Text>Be carefull! Use responsibly!</Text>
        <Text style={{fontSize: 30, fontWeight: 'bold'}}>TOTAL  IMMERSION</Text>
        <Text style={{fontSize: 30, fontWeight: 'bold'}}>MODE AHEAD !</Text>
        
        <Switch
            onValueChange = {this.props.toggleSwitch}
            value = {this.props.switchValue}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
    flex: 1,
    alignItems: 'center',
    marginTop: 100
  }
});
