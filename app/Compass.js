import React from 'react';
import { Animated, StyleSheet, Text, View, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
// import SignalIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const haversine = function(lat1,lon1,lat2,lon2){
  var R = 6371; // km
  var dLat = (lat2-lat1) * Math.PI / 180;
  var dLon = (lon2-lon1) * Math.PI / 180;
  var lat1 = (lat1) * Math.PI / 180;
  var lat2 = (lat2) * Math.PI / 180;

  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var distance = R * c;

  arc = Math.atan2(dLat , dLon) * 180 / Math.PI
  // make arc as rotation clockwise from north, not anyclockwise from east
  arc = (360 + 90/*east*/ - arc) % 360; 
  return { distance, arc };
}


export default class Compass extends React.Component {
  
  state = {
    origin: 0,
    
    angle: new Animated.Value(-45),
    
    distance: 0,
    arc: 0,

    latitude: null,
    longitude: null,
    heading: null,
    accuracy: null,
    error: null,
  }

  _mounted = false;

  lastAngle = 0;
  setAngle(angle) {
    var diff = (360 + angle - (this.lastAngle%360))%360;
    if(diff > 180) {
      diff = diff-360;
    }

    // 0.3 to calm compass a little more and make it a little more accurate
    this.lastAngle += diff * 0.3; 

    Animated.timing(
      this.state.angle,
      {
        toValue: this.lastAngle-45,                   
        duration: 200,
        useNativeDriver: true
      }
    ).start();
  }

  componentDidMount() {
    this._mounted = true;
    if(!this.props.activeLocation) {
      return;
    }

    this.recalcCompassOrigin();
    this.setAngle(0);

    this._startWatchingGpsAsync();
  }

  _startWatchingGpsAsync = async () => {
    await this._getLocationAsync();



    this.headingCallback = await Expo.Location.watchHeadingAsync(
      ({accuracy, magHeading, trueHeading}) => {
        if(!this._mounted) {
          this.headingCallback.remove();
          return;
        }

        this.setAngle(this.state.arc - trueHeading);
        this.setState({
          headingAccuracy: accuracy,
          magHeading, trueHeading
        })
      }
    );


    this.locationCallback = await Expo.Location.watchPositionAsync(
      {enableHighAccuracy: true, timeInterval: 700, distanceInterval:2},
      (position) => {
        if(!this._mounted) {
          this.locationCallback.remove();
          return;
        }
        

        const { coords } = position;

        if(coords.heading == this.state.heading &&
          coords.latitude == this.state.latitude &&
          coords.longitude == this.state.longitude
        ) {
          return;
        }

        const {arc, distance} = haversine(
          coords.latitude, coords.longitude,
          this.props.activeLocation.latitude, this.props.activeLocation.longitude
        );

        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          accuracy: coords.accuracy,
          error: null,
          arc,
          distance
        });
      }
    );
  };


  _getLocationAsync = async () => {
    const { Permissions } = Expo;
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        error: 'Permission to access location was denied',
      });
    }
  }


  componentWillUnmount() {
    this._mounted = false;
    this.headingCallback && this.headingCallback.remove();
    this.locationCallback && this.locationCallback.remove(); 
  }

  spin = this.state.angle.interpolate({
    inputRange: [0, 360],
    outputRange: ['0deg', '360deg']
  })

  recalcCompassOrigin = ev => {
    const {height, width} = Dimensions.get('window');
    this.setState({origin: (width-300)/2});
  }

  render() {
      const {height, width} = Dimensions.get('window');
      if(!this.props.activeLocation) {
        return (
          <View style={styles.container}>
            <Text style={{color: "#C00"}}>In order to get heading direction tips, you have to select marker on the map!</Text>
            <Text style={{color: "#C00"}}>You can add markers by longpressing map</Text>
          </View>
        );
      } else if(!this.state.error) {
        const signalIcons = [
          'progress-empty', 'progress-one', 'progress-two', 'progress-full'
        ]
        return (
          <View style={styles.container}>
            <Text style={{fontSize: 30, fontWeight: 'bold', textAlign: 'center', color: "#A00"}}>{this.props.activeLocation.name}</Text>
            <Animated.View
              onLayout={this.recalcCompassOrigin}
              style={{
                transform: [
                  {translateY: 30}, 
                  {rotate: this.spin},
                  {translateX: this.state.origin},// {translateY: 5},
                ],
              }}>
              <Icon name="direction" size={300} color="#F33" />
            </Animated.View>
            <Text style={{fontSize: 30, fontWeight: 'bold', textAlign: 'center', color: "#A00"}}>{Math.round(this.state.distance*1000)}m</Text>
            <Text style={{color: "#C00"}}>Position accuracy: +-{Math.round(this.state.accuracy)}m</Text>
            <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
              <Text style={{color: "#C00", marginTop: 5}}>Heading qualit: </Text>
              <Icon name={signalIcons[this.state.headingAccuracy]} size={30} color="#C00"/>
            </View>
    {/* 
            <Text>pos: {this.state.latitude} {this.state.longitude}</Text>
            <Text>heading: {this.state.trueHeading} | {this.state.magHeading}</Text>
            <Text>arc: {this.state.arc}</Text>
    */}
          </View>
        );
      } else {
        return (
          <View>
            <Text style={{color: "#C00"}}>error: {this.state.error}</Text>
          </View>
         );
      }
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
});