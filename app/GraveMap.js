import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { MapView } from 'expo';
import Dialog from "react-native-dialog";


var _activeMarkerRef = false;

export default class GraveMap extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    }
  }
 
  newLocation = {};

  showActiveMarker() {
    _activeMarkerRef && _activeMarkerRef.showCallout();
  }

  locationAddModal = (event) => {
    this.newLocation = event.nativeEvent.coordinate; // { latitude, longitute }
    
    this.setState({showModal:true});
  }

  addLocation = () => {
    this.setState({showModal:false});

    var newLocationList = JSON.parse(JSON.stringify(this.props.locations));
    newLocationList.push(this.newLocation);
    this.props.setLocations(newLocationList);
  }


  render() {
    const activeElementJson = JSON.stringify(this.props.activeLocation);
    
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: this.props.activeLocation ? this.props.activeLocation.latitude : 52.2013578,
            longitude: this.props.activeLocation ? this.props.activeLocation.longitude : 21.0338754,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421,
            
          }}
          showsUserLocation={true}
          showsMyLocationButton={true}
          showsCompass={true}
          onLongPress={this.locationAddModal}
          onMapReady={this.showActiveMarker}
        >

          {
            
            this.props.locations.map(element => {  
              const elementJson = JSON.stringify(element);
              if(elementJson == activeElementJson) {
                return (
                  <MapView.Marker
                    key={elementJson}
                    coordinate={element}  // { latitude, longitude }
                    title="Selected localization:"
                    description={element.name}
                    ref={ (element) => /*this. didnWork */_activeMarkerRef = element }
                    onPress={() => this.props.setActiveLocation(element)}
                  />
                );
              } else {
                return ( <MapView.Marker
                    key={elementJson}
                    coordinate={element}  // { latitude, longitude }
                    title="Selected localization:"
                    description={element.name}
                    onPress={() => this.props.setActiveLocation(element)}
                  />
                )
              }
            })
          }    
        {/*
          <MapView.Marker
            key="tough"
            coordinate={{
              latitude: 52.2013578,
              longitude: 21.0338754
            }}
            title="asdasd"
          >

            <MapView.Callout>
            </MapView.Callout>
          </MapView.Marker>
        */}
        </MapView> 
          
        <View>
          <Dialog.Container visible={this.state.showModal}>
            <Dialog.Title>Add new location</Dialog.Title>
            <Dialog.Description>
              Please provide name
            </Dialog.Description>
            <Dialog.Input 
              onChangeText={(text) => {this.newLocation.name = text}}
            />
            <Dialog.Button label="Cancel" onPress={() => this.setState({showModal: false})}/>
            <Dialog.Button label="Add" onPress={this.addLocation}/>
          </Dialog.Container>
        </View>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
    position: 'absolute',
//  justifyContent: 'center',

    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-start',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});
