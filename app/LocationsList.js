import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Clipboard } from 'react-native';
import { MapView } from 'expo';

export default class LocationsList extends React.Component {
  
  removeLocation = (locationToRemove) => {
    this.props.setLocations(
      this.props.locations.filter(
        location => location != locationToRemove
      )
    );
  }

  _getContactsFromClipboardAsync = async () => {
    var list = [];
    try {
      list = JSON.parse(await Clipboard.getString());
    } catch(ex){
      // ..
    }

    if(!list.length) {
      list = [];
    }
    return list;
  }

  unsafeImportList = async () => {
    this.props.setLocations(
      await this._getContactsFromClipboardAsync()
    );
  }

  concatImportList = async () => {
    this.props.setLocations(
      Array.from(
        new Set(
          [
            ...this.props.locations,
            ...(await this._getContactsFromClipboardAsync())
          ].map(x => JSON.stringify(x))
        )
      ).map(x=>JSON.parse(x))
    );
  }

  exportList = () => {
    Clipboard.setString(JSON.stringify(this.props.locations));
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        {  
          this.props.locations.length == 0 ? 
            ( 
              <View style={styles.row}>
                <Text style={{fontSize: 30, textAlign: 'center'}}>No locations on the list</Text> 
              </View>    
            ) :
            this.props.locations.map(element => {
              return (
                <View key={JSON.stringify(element)} style={styles.row}>
                  <Text style={styles.cell}>{element.name}</Text>
                  <Button title="remove" style={styles.cell}
                    onPress={() => this.removeLocation(element)}
                  />
                </View>
              );
            })
        }
          <Text style={{    
              margin: 1,
              flexDirection: 'row', 
              alignItems: 'flex-start',
              marginTop: 36
            }}>
              List import/export using clipboard
          </Text>
          <View style={styles.row}>
            <Button title="Export" 
                onPress={this.exportList}
              />
            <Button title="Import(concat)"
                onPress={this.concatImportList}
              />
            <Button title="Import"
                onPress={this.unsafeImportList}
              />
         </View>
         
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
    marginTop: 20
  },
  row: {
    margin: 1,
    flexDirection: 'row', 
    alignItems: 'flex-start',
    justifyContent: 'space-between'
  },
  cell: {
    flex: 1,
    alignSelf: 'stretch' 
  }
});
