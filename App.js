import React from 'react';
import { View, Text } from 'react-native';

import Icon from 'react-native-vector-icons/Entypo'
import BottomNavigation, { FullTab } from 'react-native-material-bottom-navigation'

import { once } from 'lodash';

import GraveMap from './app/GraveMap';
import Compass from './app/Compass';
import ARCompass from './app/ARCompass';
import LocationsList from './app/LocationsList';
import Settings from './app/Settings';


const { SecureStore } = Expo;

async function getLocationAsync() {
  const { Location, Permissions } = Expo;
  const { status } = await Permissions.askAsync(Permissions.LOCATION);
  if (status === 'granted') {
    const out =  Location.getCurrentPositionAsync({enableHighAccuracy: true});
    //console.log(out);
    return out;
  } else {
    throw new Error('Location permission not granted');
  }
}

const getLocationAsyncOnce = once(getLocationAsync);

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { 
      activeTab: this.tabs[0],
      locations: [],
      activeLocation: undefined
    };
    
    this.fetchLocationAsync();
  }

  setLocations = (locations) => {
    this.setState({locations});

    // MAYBE: handle this totally unsafe saving
    SecureStore.setItemAsync('GraveNavi.locations', JSON.stringify(locations));
  }

  setActiveLocation = (location) => {
    debugger;
    this.setState({activeLocation: location});
  }

  async fetchLocationAsync() {
    try {
      const value = await SecureStore.getItemAsync('GraveNavi.locations');

      if (value !== null){
        // We have data!
        this.setState({
          locations: JSON.parse(value)
        });
      }
      
    } catch (error) {
      console.log("Nie bangla location");
      // Error retrieving data
    }
  }

  tabs = [
    {
      key: 'map',
      icon: 'map',
      label: 'map',
      barColor: '#388E3C',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'navi',
      icon: 'direction',
      label: 'navi',
      barColor: '#B71C1C',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'list',
      icon: 'list',
      label: 'list',
      barColor: '#194AE6',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'settings',
      icon: 'tools',
      label: 'settings',
      barColor: '#E64A19',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    }
  ]
  
  renderIcon = tab => ({ isActive }) => (
    <Icon size={24} color="white" name={tab.icon} />
  )
 
  renderTab = ({ tab, isActive }) => {
    if(!tab) {
      return (
        <View></View>
      )
    } else {
      return (
      <FullTab
        isActive={isActive}
        key={tab.key}
        label={tab.label}
        renderIcon={this.renderIcon(tab)}
      />
      )
    }
  }

  renderView = () => {
    switch(this.state.activeTab.key) {
      case 'map':
        return ( 
          <GraveMap 
            locations={this.state.locations} 
            activeLocation={this.state.activeLocation} 
            setLocations={this.setLocations}
            setActiveLocation={this.setActiveLocation}
          /> 
        );
      case 'navi':
        return this.state.AROn ?
          ( <ARCompass activeLocation={this.state.activeLocation}/> ):
          ( <Compass activeLocation={this.state.activeLocation}/> )
      case 'list':
        return ( <LocationsList locations={this.state.locations} setLocations={this.setLocations}/> );
      case 'settings':
        return ( <Settings switchValue={this.state.AROn} toggleSwitch={value => this.setState({AROn: value})}/> );


      default:
        return ( <Text>{this.state.activeTab.key}</Text> );
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          { this.renderView() }
        </View>
        <BottomNavigation
          onTabPress={activeTab => this.setState({ activeTab })}
          renderTab={this.renderTab}
          tabs={this.tabs}
        />
      </View>
    )
  }
}